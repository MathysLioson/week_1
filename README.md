# Week 1: Docker history and basics

Week 1: introduction to the tool, and basic exercices.
- Introduction to the virtualization vocabulary
- Why containerization became so popular
- Why and how Docker was created
- How Docker is working with existing systems 
- How to use an existing Docker image to reproduce efficiently a compilation/execution environment  

## The virtualization family
### Sandboxing 🐣
One of the oldest example of process isolation comes from Unix v7, which introduced the chroot command in 1979. This command changes a process (and its children) root directory in the filesystem. Keyword here is the **isolation** of a ressource, to ensure its integrity or/and behavior in a given situation.

This is an early and incomplete form of containerization, called sandboxing. Today, sandboxing is still used, mainly as a security mechanism: untrusted programs or code are routinely tested and run in sandboxes to avoid harming the host operating system or machine. 

All apps on modern smartphones are running in such sandboxes. On Android, each app runs as itw own user and cannot access other 'users' files and programs. On iOS, apps are all run by 'mobile' user and chrooted to a separate filesystem.


### Containerization 🥸 
The frontier between containerization and sandboxing can be blurry. Both share the same intent, isolate processes or ressources, but modern containers are also usually easier to reproduce and manage. Containers are also a more complete way to isolate some content, with the capacity to even abstract the platform OS and mimic a different version or system entirely. 

A container can be used to: 
- Quickly share and setup a development environment.
- Provide a compatible space to run specific applications.
- Safely interact with unstrusted programs or code.
- Automate parts of any software lifecyle.

### Virtualization 🤖
Virtualization is the ultimate answer to fix software biggest issue: ensure it will work as intended. By simulating a whole system, down to its hardware components, developpers can ensure the program execution will (maybe) go as planned. The virtualized system, called a virtual machine (VM), is run by a separate software layer called a hypervisor, which enables several OS to run side-by-side and manage available ressources.
Virtualization shares a lot of containers advantages, but pushes the portability of programs even further by asbtracting hardware and platform. 
Want to run a cross-compiled ARM binary on your x84/x64 based desktop? Use a virtual machine. Want to keep your old family computer but it takes forever to boot up? Make it [ascend and leave behind its mortal body](https://www.makeuseof.com/tag/turn-missioncritical-pc-vm-dies/) using virtualization.

VMs are the most complete form of virtualization available, but the extra cost of simulating hardware components and complexity gain means a performance hit. Containers are preferred for most applications where abstracting only a part of the OS is sufficient.

## Why containerization became so popular
### The dark ages
Early computers could only run a single process, which was able to access all the hardware ressources. As their computation power grew, it became possible to run several programs at once and switch the execution context, which was one of the main feature Unix was designed for: user-spaces were created to limit access to critical ressources, reserved by the root user, and segregate data by user.

### Welcome to jail
Until the mid 2000s, the main source of containerization was chrooting processes into a **chroot jail**, hiding the system ressources out of the selected path. 
Here is a [quick example](https://www.cyberciti.biz/faq/unix-linux-chroot-command-examples-usage-syntax/) of chroot usage:
```
# Create chroot folder
J=$HOME/jail
mkdir -p $J
mkdir -p $J/{bin,lib64,lib}
cd $J

# Copy the commands to use in this isolated environment
cp -v /bin/{bash,ls} $J/bin

# Copy bin and ls binaries dependencies (e.g. libc.so.6), using ldd
bash_deps="$(ldd /bin/bash | egrep -o '/lib.*\.[0-9]')"
for i in $bash_deps; do mkdir -p "${J}$(dirname $i)" ; cp  -v "$i" "${J}${i}"; done

ls_deps="$(ldd /bin/ls | egrep -o '/lib.*\.[0-9]')"
for i in $ls_deps; do mkdir -p "${J}$(dirname $i)" ; cp  -v "$i" "${J}${i}"; done
```
Now this brand new jail can be used:
```
sudo chroot $J /bin/bash
ls /
ls /etc
ls /var
```
![alt text](pictures/chroot_example.png)

[Busybox](https://busybox.net/) is commonly used [with chroot](https://lists.gnu.org/archive/html/dejagnu/2003-06/msg00088.html) to quickly create sandboxed environment, as it includes most of Unix standard tools. It was designed in the 90s to be a minimal core user space for embedded linux systems. 

### Splitting the atom
The first simultaneous multithreading desktop processor implementation was realized on Intel Pentium 4 (3GHz) in 2000, which Intel called Hyper-threading.
The first multi-core processor available to the public was the POWER4 (1.1GHz) by IBM, in 2001. Multi-core architecture only became mainstream in the late 2000's years, with chips like the Pentium D by Intel and the Athlon 64 X2 (2005). The Pentium D was made of 2 Pentium 4 core, but (unlike the Athlon) did not support Hyper-threading!

Between 2005 and 2010, adoption of multi-core architecture exploded. In 2008, Single core CPUs shipments only [made 10% of its shipments](https://www.networkworld.com/article/2283812/shift-to-multicore-processors-inevitable--but-enterprises-face-challenges.html), and quad-core CPUs were already the majority of Intel sales.

At this point CPU frequency was not increasing as fast as it did between 1995 (~100MHz) and 2005 (~3GHz), and the adoption of multi-core architectures as a workaround had several side-effects:
- Computation power on a single machine exploded.
- Machines were increasingly able to do more than one thing at once.
![alt text](pictures/cpu_perfs.png)
*Image: Karl Rupp*

### The rising problem of software complexity
At this point in time, software engineering was experiencing growing pains, scaling with computer systems becoming more distributed than ever. The formerly standard model of an application contained in a single binary/process transformed to a multi-agents, multi-services application running on different machines with increasing complexity.
The following issues emerged from this:
- How to develop, [test](https://www.atlassian.com/fr/continuous-delivery/continuous-integration), debug and maintain a complex application with platform dependent behaviors? 😕
- How to [ship it](https://www.atlassian.com/fr/continuous-delivery) and ensure it runs as intended on the target? 🤔
- How to control cluster installations and server farms?
- How to monitor the life cycle of all the agents interacting together within the application scope? And restart one of them if needed? 🥵

### Rise of container libs
Several libraries and software were designed to create and manage contained environment in a more convenient way:
- VMware (1999) version 1 could run instances of MS-DOS 6, W95/98, etc without an hypervisor (which only became popular after 2005).
- Linux VServer (2001), which patched the Linux kernel to allow OS virtualization.
- Solaris containers (2004), one of the earliest to provide snapshots and cloning of it 'zones' as a feature.
- Open VZ (2005) could only run Linux, but does not use a true hypervisor which allows him to be more efficient, and also supported snapshots.
- Process containers (2006): originally a Google project, it was renamed Control Groups (**cgroups**) in 2007 and merged into the Linux kernel 2.6.24.
- Hyper-V (2008), Microsoft virtualization solution.
- LinuX Containers or **LXC** (2008): first complete Linux container manager, based on kernel cgroups and namespaces, it does not require kernel patches to work. 

All these libraries and software were trying to provide virtualization/containerization methods, but most were expensive to run (and buy!) and sometimes required the host OS to go through invasive modifications to support the virtualization features.

## Enters Docker 👑

### The ideal out-of-the-box container manager
![alt text](pictures/finally.png)

Did you know this is Docker first mention since the title section? That was a lot to unpack.
The company it came from, DotCloud, was founded by **Kamel Founadi**, **Solomon Hykes**, and **Sebastien Pahl** in Paris in 2008. The docker tool was first publicly introduced to the public by Hykes at the **March 2013** Santa Clara **PyCon** as an open-source, **LXC** based container manager. 

Here is the link to the moment where Solomon Hykes, 29, made history:
https://www.youtube.com/watch?v=wW9CAH9nSLs

Solomon Hykes, in front of a crowd and within 5 minutes, launches and handles multiple containers with separate filesystems, network interfaces, log capture, using a single utility called...docker 🤯.

If you want to know more about the intent behind Docker, from the man himself:
https://www.youtube.com/watch?v=3N3n9FzebAA

It was renamed *Docker, Inc* in September 2013, when DotCloud started collaborating with Red Hat. With the [0.9](https://docs.docker.com/engine/release-notes/prior-releases/#090-2014-03-10) release in March 2014, Docker ditched the LXC to use its own Go implementation, [libcontainer](https://github.com/opencontainers/runc/tree/master/libcontainer).

In March 2015, according to cloud infrastructure monitoring company [Datadog](https://www.datadoghq.com/docker-adoption/), already 15% of its customer had switched to Docker. 

As of 2020, [activity](https://www.docker.com/blog/docker-index-shows-continued-massive-developer-adoption-and-activity-to-build-and-share-apps-with-docker/) around Docker and associated tools is still quickly growing:

![alt text](pictures/docker_stats.png)
*Image: docker.com*

### Automation based on docker

The simplicity and flexibility of Docker made it spread quickly in the computer science community and industry, and in parallel to Docker development, other tools quickly expanded on its features to provide an additional layer of infrastructure abstraction:

-**Docker Compose**, which loads and runs YAML configuration files describing multi-container docker applications. The first public beta was released in December 2013 (0.0.1) and the first production-ready version in October 2014 (1.0).

-**Docker Swarm** is an orchestration tool designed to work in cloud infrastructures. Its main feature is the ability to pull several Docker engines, called nodes, into a single virtual entity. Containers can communicate using the Docker API. First version was released in 2014.

-**Kubernetes** is also an orchestration tool, but while Docker Swarm is the light-weight, beginner-friendly option, Kubernetes automation capacity is superior:
-	Support for every operating systems
-	Able to handle very complex architectures and large payloads
-	Fully automated with native monitoring and self-healing features
-	Official support from the 3 main cloud providers: Google, Azure and AWS
Kubernetes 1.0 was released in July 2015 by Google, as a collaboration work with the Linux. As of January 2021, Kubernetes is still the [38th](https://github.com/EvanLi/Github-Ranking/blob/master/Top100/Top-100-stars.md) most starred project on Github, and second in Go language projects, only behind...Go language project itself. In 2017, it was [one](https://www.cncf.io/blog/2018/03/06/kubernetes-first-cncf-project-graduate/) of the fastest growing Open Source projects.

## What is docker made of
### Useful resources
-[Docker official starting guide](https://docs.docker.com/get-started/overview/)

-[Docker curriculum tutorial](https://docker-curriculum.com/)

-[Docker commands cheat sheet](https://www.docker.com/sites/default/files/d8/2019-09/docker-cheat-sheet.pdf) (Hang it above your bed!)

### Internal structure
Docker is made of 3 main components:

-The **Docker daemon (or engine)**. It listens to Docker API requests and manage docker objects: images, containers, networks and volumes. It can also communicate with other Docker daemons using the same API. As of January 2021, the latest version is 20.10.

-The **Docker client**. This is the main way for users to interact with Docker directly. All *docker X* commands are forwarded by the client to the Docker daemon with the Docker API. A client can communicate with more than one daemon.

-The **Docker registry** is the component storing images. Docker has a public registry at hub.docker.com, but several organizations provide one, like [GitLab](https://docs.gitlab.com/ee/user/packages/container_registry/) for its public repositories. Fun fact: you can pull the *registry* image and quickly launch your own, local Docker registry 🤓.

![alt text](pictures/docker_architecture.svg)
*Image: docs.docker.com*
![alt text](pictures/docker_struct.png)

*Image: https://stackoverflow.com/questions/41645665/how-containerd-compares-to-runc*

### Docker objects
-**Image**: read-only template used to instantiate containers. A custom image can be based on an existing one. An image description can be reduced to a series of instruction in a file called **Dockerfile**. Built image is stored using a layer system and lazy evaluation for these layers, which means common layers are not only shared, but rebuilding them is a step which can be skipped if the Dockerfile content stayed the same. This is one of Docker main assets: small and fast images. 

-**Containers**: running instance of an image. Handled using the Docker API: run, stop, move, kill, etc. You can connect a shell to an existing one, add external storage to it (see volumes), and set in details its configuration. Any **changes to a container are lost when removed** if not stored in a persistent storage hooked to the container!

-**Volumes**: the [preferred](https://docs.docker.com/storage/volumes/) mechanism to store persisting data generated or used by containers. They are similar to the binding option, but unlike the later they are not platform-specific and are easier to handle/migrate. 

### Container standard components
In the beginning, everybody aligned themselves on Docker standard for container handling and image format. Everything was perfect. Or was it? 🤨

-Docker **required to run containers as root** (up to 1.10 in Feb 2016) and had a few other security concerns, like the signature verification of public registry images.

-Docker was monolithic, the **same daemon was in charge of everything**. If the daemon was killed, containers disappeared (The docker engine was separated from the docker daemon managing containers in 1.11 Docker version in June 2016).

-Docker was using its own **proprietary image format**.

One of the first serious rival to Docker was CoreOS's **rkt** 🚀 in 2014: it provided an open-source image format, App container Image (ACI), and container runtime (rkt).
In June 2015, the container industry main actors agreed on the creation of a common image format and container runtime, through the **Open Container Initiative (OCI)**. Docker gave its container runtime to the OCI: **runc**.
The OCI standard defined the container image as:
- A layered filesystem, where each layer is stored as a diff from the previous layer.
- An image manifest, made of the layers list for a given platform.
- A manifest index, containing the list of image manifests for each plateform it supports.
- An image configuration: env vars, exposed ports, default entry command, etc.

These images are created by a builder, e.g. Docker, Buildah or Kaniko. 
The creation of a common image format and container runtime allowed concurrents companies to implement alternative modules for each part of the model popularized with docker:

![alt text](pictures/container-ecosystem.drawio.png)

*Image: Tutorial Works*

## Learning docker client interface
So now you are filled to the brim with theory. Let's start typing commands.

![alt text](pictures/peacock.jpg)

### docker monitoring commands
This section covers the following subcommands:
- history
- image
- info
- inspect
- ps
- tag

#### docker info

One command can give you almost all useful info about the docker engine overview:

`docker info`

```
raph@raph-VirtualBox:~/iut/week_1$ docker info
Client:
 Context:    default
 Debug Mode: false

Server:
 Containers: 2
  Running: 0
  Paused: 0
  Stopped: 2
 Images: 3
 Server Version: 20.10.8
 Storage Driver: overlay2
  Backing Filesystem: extfs
  Supports d_type: true
  Native Overlay Diff: true
  userxattr: false
 Logging Driver: json-file
 Cgroup Driver: cgroupfs
 Cgroup Version: 1
 Plugins:
  Volume: local
  Network: bridge host ipvlan macvlan null overlay
  Log: awslogs fluentd gcplogs gelf journald json-file local logentries splunk syslog
 Swarm: inactive
 Runtimes: io.containerd.runc.v2 io.containerd.runtime.v1.linux runc
 Default Runtime: runc
 Init Binary: docker-init
 containerd version: e25210fe30a0a703442421b0f60afac609f950a3
 runc version: 
 init version: de40ad0
 Security Options:
  apparmor
  seccomp
   Profile: default
 Kernel Version: 5.8.0-50-generic
 Operating System: Ubuntu Core 18
 OSType: linux
 Architecture: x86_64
 CPUs: 1
 Total Memory: 7.772GiB
 Name: raph-VirtualBox
 ID: 5LGA:CIHF:5CLK:JE45:RDU2:VNMC:NZPH:VAVH:TYX5:4P5X:7KYE:X56D
 Docker Root Dir: /var/snap/docker/common/var-lib-docker
 Debug Mode: false
 Registry: https://index.docker.io/v1/
 Labels:
 Experimental: false
 Insecure Registries:
  127.0.0.0/8
 Live Restore Enabled: false
 ```

This command will give you: 
- The amount of containers in each state (running, paused, etc) 
- The number of images you pulled locally
- The docker engine version
- The drivers you are using for the volumes and images storage (overlay2 here)
- Your machine architecture details
- The container runtime: default is Docker runc, but you could change it and [use crun](https://github.com/containers/crun/issues/37) instead (Podman's container runtime)
- A few configuration variables

Docker client interface has been designed to be the least intrusive on existing systems, and blend with other Unix [typical](https://clig.dev/) syntax:

`docker SUB_CMD [OPTIONS]`

#### docker image

There is an exception: `docker image`

This command is so common you can skip the image sub-command, and docker will just assume you meant to add it:

```
raph@raph-VirtualBox:~/iut/week_1$ docker image

Usage:  docker image COMMAND

Manage images

Commands:
  build       Build an image from a Dockerfile
  history     Show the history of an image
  import      Import the contents from a tarball to create a filesystem image
  inspect     Display detailed information on one or more images
  load        Load an image from a tar archive or STDIN
  ls          List images
  prune       Remove unused images
  pull        Pull an image or a repository from a registry
  push        Push an image or a repository to a registry
  rm          Remove one or more images
  save        Save one or more images to a tar archive (streamed to STDOUT by default)
  tag         Create a tag TARGET_IMAGE that refers to SOURCE_IMAGE

Run 'docker image COMMAND --help' for more information on a command.
raph@raph-VirtualBox:~/iut/week_1$ docker build
"docker build" requires exactly 1 argument.
See 'docker build --help'.

Usage:  docker build [OPTIONS] PATH | URL | -

Build an image from a Dockerfile
raph@raph-VirtualBox:~/iut/week_1$ docker history ubuntu
IMAGE          CREATED       CREATED BY                                      SIZE      COMMENT
d13c942271d6   2 weeks ago   /bin/sh -c #(nop)  CMD ["bash"]                 0B        
<missing>      2 weeks ago   /bin/sh -c #(nop) ADD file:122ad323412c2e70b…   72.8MB    
raph@raph-VirtualBox:~/iut/week_1$ docker image history ubuntu
IMAGE          CREATED       CREATED BY                                      SIZE      COMMENT
d13c942271d6   2 weeks ago   /bin/sh -c #(nop)  CMD ["bash"]                 0B        
<missing>      2 weeks ago   /bin/sh -c #(nop) ADD file:122ad323412c2e70b…   72.8MB    
raph@raph-VirtualBox:~/iut/week_1$ docker image build
"docker image build" requires exactly 1 argument.
See 'docker image build --help'.

Usage:  docker image build [OPTIONS] PATH | URL | -

Build an image from a Dockerfile
raph@raph-VirtualBox:~/iut/week_1$ docker build
"docker build" requires exactly 1 argument.
See 'docker build --help'.

Usage:  docker build [OPTIONS] PATH | URL | -

Build an image from a Dockerfile
```

If you try a few of docker image options, you may have noticed an exception to this rule:
```
raph@raph-VirtualBox:~/iut/week_1$ docker image ls
REPOSITORY    TAG       IMAGE ID       CREATED        SIZE
ubuntu        latest    d13c942271d6   2 weeks ago    72.8MB
busybox       latest    beae173ccac6   4 weeks ago    1.24MB
hello-world   latest    feb5d9fea6a5   4 months ago   13.3kB
raph@raph-VirtualBox:~/iut/week_1$ docker ls
docker: 'ls' is not a docker command.
See 'docker --help'
```

This command is so common it has its own shortcut: `docker images`

```
raph@raph-VirtualBox:~/iut/week_1$ docker images
REPOSITORY    TAG       IMAGE ID       CREATED        SIZE
ubuntu        latest    d13c942271d6   2 weeks ago    72.8MB
busybox       latest    beae173ccac6   4 weeks ago    1.24MB
hello-world   latest    feb5d9fea6a5   4 months ago   13.3kB
```

From left to right, you can see:
- The image **human-friendly name**, prefixed with the registry URL it came from. When hidden, the image comes from Docker official registry, **Docker Hub** at https://index.docker.io/.
- The image **version**. When not specified, docker will assume you meant **latest**. [Never, ever use the latest tag](https://vsupalov.com/docker-latest-tag/) in automated processes. It's just a default tag, and a confusing one.
- The **image ID**. It is the SHA256 hash of the image configuration object, which contains the SHA256 hashes of the image layers:
	![alt text](pictures/layers.png)
	*Image: windsock.io*
- The creation date. Not the pulling date, but the one at `docker build` call.
- The image size. Since OCI is a layer-based system, the sum of local docker images is not the actual space used on the local disk:
	```
	raph@raph-VirtualBox:~/iut/week_1$ docker pull python:3.9.10-alpine3.15
	3.9.10-alpine3.15: Pulling from library/python
	59bf1c3509f3: Pull complete 
	07a400e93df3: Pull complete 
	3617a104f972: Pull complete 
	9e5d66ce26b5: Pull complete 
	4672bfb9aa95: Pull complete 
	Digest: sha256:72203553f0358f3dd79e07fe5d01ef5efb0fea261364a46e266c690f9c28e0b5
	Status: Downloaded newer image for python:3.9.10-alpine3.15
	docker.io/library/python:3.9.10-alpine3.15
	raph@raph-VirtualBox:~/iut/week_1$ docker pull alpine:3.15
	3.15: Pulling from library/alpine
	59bf1c3509f3: Already exists 
	Digest: sha256:21a3deaa0d32a8057914f36584b5288d2e5ecc984380bc0118285c70fa8c9300
	Status: Downloaded newer image for alpine:3.15
	docker.io/library/alpine:3.15
	```
	In the example above, *python* image version *3.9.10-alpine3.15* first layer is the linux *alpine* image version 3.15, so the second call to docker pull never actually downloaded it, because a local layer already had a matching hash.
	*How do you know this?*
	You can check the [Dockerfile](https://github.com/docker-library/python/blob/6a2c0f48f011aa279a0c9190725fc84a220460bc/3.9/alpine3.15/Dockerfile) used to build this python image on Docker Hub.
	![alt text](pictures/python_dockerfile.png)
	
	Or you can use docker history.
	
#### docker history
```
raph@raph-VirtualBox:~/iut/week_1$ docker history alpine:3.15
IMAGE          CREATED        CREATED BY                                      SIZE      COMMENT
c059bfaa849c   2 months ago   /bin/sh -c #(nop)  CMD ["/bin/sh"]              0B
<missing>      2 months ago   /bin/sh -c #(nop) ADD file:9233f6f2237d79659…   5.59MB    
```
Here the last line before the entry point definition (CMD...) is the same as the first line in python image layer list:
```
raph@raph-VirtualBox:~/iut/week_1$ docker history python:3.9.10-alpine3.15
IMAGE          CREATED        CREATED BY                                      SIZE      COMMENT
e49e2f1d4108   8 days ago     /bin/sh -c #(nop)  CMD ["python3"]              0B 
<missing>      8 days ago     /bin/sh -c set -ex;   wget -O get-pip.py "$P…   8.31MB    
<missing>      8 days ago     /bin/sh -c #(nop)  ENV PYTHON_GET_PIP_SHA256…   0B 
<missing>      8 days ago     /bin/sh -c #(nop)  ENV PYTHON_GET_PIP_URL=ht…   0B 
<missing>      8 days ago     /bin/sh -c #(nop)  ENV PYTHON_SETUPTOOLS_VER…   0B 
<missing>      8 days ago     /bin/sh -c #(nop)  ENV PYTHON_PIP_VERSION=21…   0B 
<missing>      8 days ago     /bin/sh -c cd /usr/local/bin  && ln -s idle3…   32B 
<missing>      8 days ago     /bin/sh -c set -ex  && apk add --no-cache --…   32.6MB    
<missing>      8 days ago     /bin/sh -c #(nop)  ENV PYTHON_VERSION=3.9.10    0B 
<missing>      8 weeks ago    /bin/sh -c #(nop)  ENV GPG_KEY=E3FF2839C048B…   0B 
<missing>      8 weeks ago    /bin/sh -c set -eux;  apk add --no-cache   c…   1.82MB    
<missing>      8 weeks ago    /bin/sh -c #(nop)  ENV LANG=C.UTF-8             0B 
<missing>      8 weeks ago    /bin/sh -c #(nop)  ENV PATH=/usr/local/bin:/…   0B 
<missing>      2 months ago   /bin/sh -c #(nop)  CMD ["/bin/sh"]              0B 
<missing>      2 months ago   /bin/sh -c #(nop) ADD file:9233f6f2237d79659…   5.59MB
```
	
By the way, these **missing** entries are [not an error](https://windsock.io/explaining-docker-image-ids/) from the image: in short, Docker changed its indexing system with the 1.10 release. Images and layers have been decoupled, and an image is now defined as a manifest of layers hashes.

#### docker inspect
You can also display the image layers with the `docker inspect` tool.
Here you will notice something strange:
```
raph@raph-VirtualBox:~/iut/week_1$ docker inspect alpine:3.15 | grep Layers -A 2
            "Layers": [
                "sha256:8d3ac3489996423f53d6087c81180006263b79f206d3fdec9e66f0e27ceb8759"
            ]
raph@raph-VirtualBox:~/iut/week_1$ docker inspect python:3.9.10-alpine3.15 | grep Layers -A 5
            "Layers": [
                "sha256:8d3ac3489996423f53d6087c81180006263b79f206d3fdec9e66f0e27ceb8759",
                "sha256:f57e81d89e60f9580c57db2e677a49273fd960f729a7b90d9a397d54ef554297",
                "sha256:7c47301c002d18815b69231b4d4afaad5af33215fc984fe0f2ac0da3315e1365",
                "sha256:953ae22fd98f16c781ecc661587877907b59cbe28d428fb2afd17f648538c67b",
                "sha256:3a8f965bcc9c2382a40b01768f0650730e78f23285ca65db60409e145bf5243a"
```
docker inspect sees a different number of layers than docker history! Some docker instructions in the Dockerfile, like **CMD**, are producing metadata for the image, and since it does not add actual content, the diff is empty.

#### docker tag

Several images can shared the image ID, and you can use the `docker tag` command to change the string value in the REPOSITORY column:
```
raph@raph-VirtualBox:~/iut/week_1$ docker tag ubuntu ubuntu_copy
raph@raph-VirtualBox:~/iut/week_1$ docker images
REPOSITORY    TAG                 IMAGE ID       CREATED        SIZE
python        3.9.10-alpine3.15   e49e2f1d4108   8 days ago     48.4MB
ubuntu_copy   latest              d13c942271d6   2 weeks ago    72.8MB
ubuntu        latest              d13c942271d6   2 weeks ago    72.8MB
```

Think of tag as a subcommand to create aliases for images ID. A very common usage is to use tag to change the image URL prefix and push to a different docker registry.

#### docker ps
Listing containers is done using the `docker ps` command:
```
raph@raph-VirtualBox:~/iut/week_1$ docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
```

By default, it only shows the containers in a non-exited state:

```
raph@raph-VirtualBox:~/iut/week_1$ docker ps -a
CONTAINER ID   IMAGE         COMMAND    CREATED       STATUS                   PORTS     NAMES
505e15fc9fac   ubuntu        "bash"     4 hours ago   Exited (0) 3 hours ago             eager_chandrasekhar
9c9415d43642   hello-world   "/hello"   3 days ago    Exited (0) 3 hours ago             hungry_faraday
```

The NAMES column contains a generated name with the *adjective_noun* format to provide a human friendly, which you can use in place of the CONTAINER ID entry.

### docker pull / push

You can upload/download images to/from registries with `docker push` and `docker pull`. Nothing to see here, especially if you already know about *git*.

### docker run / kill, start and stop

You can run a shell in a linux container like below:
```
raph@raph-VirtualBox:~/iut/week_1$ docker run -it ubuntu:16.04
root@bf69d3bb1f02:/# whoami
root
root@bf69d3bb1f02:/# exit
exit
raph@raph-VirtualBox:~/iut/week_1$ 
```

You can also stop, start, kill the existing containers. In the example below, a container is created with an endless task:
```
raph@raph-VirtualBox:~/iut/week_1$ docker run -d busybox /bin/sh -c "while true; do sleep 1 ; echo 'hello world' ; done"
9c567491f06400d937ecbc29c1e4f6e7e5c14dc9363854ea7f17f2da357350fa
raph@raph-VirtualBox:~/iut/week_1$ docker ps
CONTAINER ID   IMAGE     COMMAND                  CREATED         STATUS        PORTS     NAMES
9c567491f064   busybox   "/bin/sh -c 'while t…"   3 seconds ago   Up 1 second             clever_ramanujan
```

`docker run` has a lot of options. In the example above, -d means to create the container in a detached state, and leave it in the background. **Be careful to put all the options before the image name!**
To suspend a container, you can use the `docker pause` command:
```
raph@raph-VirtualBox:~/iut/week_1$ docker pause clever_ramanujan 
clever_ramanujan
raph@raph-VirtualBox:~/iut/week_1$ docker ps
CONTAINER ID   IMAGE     COMMAND                  CREATED          STATUS                   PORTS     NAMES
9c567491f064   busybox   "/bin/sh -c 'while t…"   13 seconds ago   Up 12 seconds (Paused)             clever_ramanujan
```

Or `docker kill`, to destroy it:

```
raph@raph-VirtualBox:~/iut/week_1$ docker kill 9c567491f064
9c567491f064
raph@raph-VirtualBox:~/iut/week_1$ docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
raph@raph-VirtualBox:~/iut/week_1$
```

### docker save / load
You can save docker images as tar archives with `docker save`:
```
raph@raph-VirtualBox:~/iut/week_1$ docker images
REPOSITORY    TAG                 IMAGE ID       CREATED        SIZE
hello-world   latest              feb5d9fea6a5   4 months ago   13.3kB
```
```
raph@raph-VirtualBox:~/iut/week_1$ docker save hello-world:latest > myHelloImg.tar
raph@raph-VirtualBox:~/iut/week_1$ ls
myHelloImg.tar  pictures  README.md
```

Let's this image from the list of local images with `docker rmi`:

```
raph@raph-VirtualBox:~/iut/week_1$ docker rmi -f hello-world:latest 
Untagged: hello-world:latest
Untagged: hello-world@sha256:975f4b14f326b05db86e16de00144f9c12257553bba9484fed41f9b6f2257800
Deleted: sha256:feb5d9fea6a5e9606aa995e879d862b825965ba48de054caab5ef356dc6b3412
Deleted: sha256:e07ee1baac5fae6a26f30cabfe54a36d3402f96afda318fe0a96cec4ca393359
raph@raph-VirtualBox:~/iut/week_1$ docker images
REPOSITORY    TAG                 IMAGE ID       CREATED        SIZE
```

Then you can load it with the symetrical operation `docker load`:

```
raph@raph-VirtualBox:~/iut/week_1$ docker load < myHelloImg.tar
e07ee1baac5f: Loading layer [==================================================>]  14.85kB/14.85kB
Loaded image: hello-world:latest
```

And we can see the image is back in the list of loaded images:

```
raph@raph-VirtualBox:~/iut/week_1$ docker images
REPOSITORY    TAG                 IMAGE ID       CREATED        SIZE
hello-world   latest              feb5d9fea6a5   4 months ago   13.3kB
```

### Share files with containers

For this week, you can simply copy the following syntax when creating a container to share a folder between host and containers:
```
docker run --mount type=bind, src="path/in/host", dst="path/in/container" ...
```

## A practical example

### Cross-compiling attempt

Let's imagine we need to build a very simple binary. On Debian, it would be as simple as:
```
gcc main.c -o hello_host
raph@raph-VirtualBox:~/iut/week_1$ ./hello_host
Hello, world!gcc detected version 10.2
```

And then, a quick exam of this binary would say what platform it is made for:
```
raph@raph-VirtualBox:~/iut/week_1$ file hello_host 
hello_host: ELF 64-bit LSB shared object, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, BuildID[sha1]=0bd7e16aeef63bae4856dc7c9afb992a6139f3d3, for GNU/Linux 3.2.0, not stripped
```

But what if we wanted to compile it for an ARM platform? 
And here we can already hear someone saying:

-"But...there is no need for docker, just install the Debian package."

*Oh boy, I have a bad feeling about this.*

-"Ok, let's give it a try."

```
aph@raph-VirtualBox:~/iut/week_1$ sudo aptitude install gcc-arm-linux-gnueabi
[sudo] password for raph: 
The following NEW packages will be installed:
  binutils-arm-linux-gnueabi{a} cpp-10-arm-linux-gnueabi{a} cpp-arm-linux-gnueabi{a} gcc-10-arm-linux-gnueabi{a} 
  gcc-10-arm-linux-gnueabi-base{a} gcc-10-cross-base{a} gcc-arm-linux-gnueabi libasan6-armel-cross{a} libatomic1-armel-cross{a} 
  libc6-armel-cross{a} libc6-dev-armel-cross{a} libgcc-10-dev-armel-cross{a} libgcc-s1-armel-cross{a} libgomp1-armel-cross{a} 
  libstdc++6-armel-cross{a} libubsan1-armel-cross{a} linux-libc-dev-armel-cross{a} 
The following packages will be REMOVED:
  libyajl2{u} tini{u} 
0 packages upgraded, 17 newly installed, 2 to remove and 89 not upgraded.
Need to get 31,3 MB of archives. After unpacking 107 MB will be used.
Do you want to continue? [Y/n/?] 
Err http://fr.archive.ubuntu.com/ubuntu groovy/universe amd64 gcc-10-arm-linux-gnueabi-base amd64 10.2.0-8ubuntu1cross1
  404  Not Found [IP: 51.158.154.169 80]
Err http://fr.archive.ubuntu.com/ubuntu groovy/universe amd64 cpp-10-arm-linux-gnueabi amd64 10.2.0-8ubuntu1cross1
  404  Not Found [IP: 51.158.154.169 80]
Err http://fr.archive.ubuntu.com/ubuntu groovy/universe amd64 cpp-arm-linux-gnueabi amd64 4:10.2.0-1ubuntu1
  404  Not Found [IP: 51.158.154.169 80]
Err http://fr.archive.ubuntu.com/ubuntu groovy/universe amd64 binutils-arm-linux-gnueabi amd64 2.35.1-1ubuntu1
  404  Not Found [IP: 51.158.154.169 80]
Ign http://fr.archive.ubuntu.com/ubuntu groovy/main amd64 gcc-10-cross-base all 10.2.0-8ubuntu1cross1
Ign http://fr.archive.ubuntu.com/ubuntu groovy/universe amd64 libc6-armel-cross all 2.32-0ubuntu3cross2
Ign http://fr.archive.ubuntu.com/ubuntu groovy/universe amd64 libgcc-s1-armel-cross all 10.2.0-8ubuntu1cross1
Ign http://fr.archive.ubuntu.com/ubuntu groovy/universe amd64 libgomp1-armel-cross all 10.2.0-8ubuntu1cross1
Ign http://fr.archive.ubuntu.com/ubuntu groovy/universe amd64 libatomic1-armel-cross all 10.2.0-8ubuntu1cross1
Ign http://fr.archive.ubuntu.com/ubuntu groovy/universe amd64 libasan6-armel-cross all 10.2.0-8ubuntu1cross1
Ign http://fr.archive.ubuntu.com/ubuntu groovy/universe amd64 libstdc++6-armel-cross all 10.2.0-8ubuntu1cross1
Ign http://fr.archive.ubuntu.com/ubuntu groovy/universe amd64 libubsan1-armel-cross all 10.2.0-8ubuntu1cross1
Ign http://fr.archive.ubuntu.com/ubuntu groovy/universe amd64 libgcc-10-dev-armel-cross all 10.2.0-8ubuntu1cross1
Err http://fr.archive.ubuntu.com/ubuntu groovy/universe amd64 gcc-10-arm-linux-gnueabi amd64 10.2.0-8ubuntu1cross1
  404  Not Found [IP: 51.158.154.169 80]
Err http://fr.archive.ubuntu.com/ubuntu groovy/universe amd64 gcc-arm-linux-gnueabi amd64 4:10.2.0-1ubuntu1
  404  Not Found [IP: 51.158.154.169 80]
Ign http://fr.archive.ubuntu.com/ubuntu groovy/universe amd64 linux-libc-dev-armel-cross all 5.8.0-19.20cross2
Ign http://fr.archive.ubuntu.com/ubuntu groovy/universe amd64 libc6-dev-armel-cross all 2.32-0ubuntu3cross2
Err http://fr.archive.ubuntu.com/ubuntu groovy/main amd64 gcc-10-cross-base all 10.2.0-8ubuntu1cross1
  404  Not Found [IP: 51.158.154.169 80]
Err http://fr.archive.ubuntu.com/ubuntu groovy/universe amd64 libc6-armel-cross all 2.32-0ubuntu3cross2
  404  Not Found [IP: 51.158.154.169 80]
Err http://fr.archive.ubuntu.com/ubuntu groovy/universe amd64 libgcc-s1-armel-cross all 10.2.0-8ubuntu1cross1
  404  Not Found [IP: 51.158.154.169 80]
Err http://fr.archive.ubuntu.com/ubuntu groovy/universe amd64 libgomp1-armel-cross all 10.2.0-8ubuntu1cross1
  404  Not Found [IP: 51.158.154.169 80]
Err http://fr.archive.ubuntu.com/ubuntu groovy/universe amd64 libatomic1-armel-cross all 10.2.0-8ubuntu1cross1
  404  Not Found [IP: 51.158.154.169 80]
Err http://fr.archive.ubuntu.com/ubuntu groovy/universe amd64 libasan6-armel-cross all 10.2.0-8ubuntu1cross1
  404  Not Found [IP: 51.158.154.169 80]
Err http://fr.archive.ubuntu.com/ubuntu groovy/universe amd64 libstdc++6-armel-cross all 10.2.0-8ubuntu1cross1
  404  Not Found [IP: 51.158.154.169 80]
Err http://fr.archive.ubuntu.com/ubuntu groovy/universe amd64 libubsan1-armel-cross all 10.2.0-8ubuntu1cross1
  404  Not Found [IP: 51.158.154.169 80]
Err http://fr.archive.ubuntu.com/ubuntu groovy/universe amd64 libgcc-10-dev-armel-cross all 10.2.0-8ubuntu1cross1
  404  Not Found [IP: 51.158.154.169 80]
Err http://fr.archive.ubuntu.com/ubuntu groovy/universe amd64 linux-libc-dev-armel-cross all 5.8.0-19.20cross2
  404  Not Found [IP: 51.158.154.169 80]
Err http://fr.archive.ubuntu.com/ubuntu groovy/universe amd64 libc6-dev-armel-cross all 2.32-0ubuntu3cross2
  404  Not Found [IP: 51.158.154.169 80]
0% [Working]E: Failed to fetch http://fr.archive.ubuntu.com/ubuntu/pool/universe/g/gcc-10-cross/gcc-10-arm-linux-gnueabi-base_10.2.0-8ubuntu1cross1_amd64.deb: 404  Not Found [IP: 51.158.154.169 80]
E: Unable to fetch some packages; try '-o APT::Get::Fix-Missing=true' to continue with missing packages
```

As a matter of fact, this part was tested on an old Ubuntu VM. 

```
raph@raph-VirtualBox:~/iut/week_1$ cat /etc/os-release 
NAME="Ubuntu"
VERSION="20.10 (Groovy Gorilla)"
```

Support ended in June, 2021. and apt remote sources were moved to an archived position. So now I need to modify apt sources.list, update and try again...

Meanwhile, in docker land:

```
raph@raph-VirtualBox:~/iut/week_1$ docker pull stronglytyped/arm-none-eabi-gcc
Using default tag: latest
latest: Pulling from stronglytyped/arm-none-eabi-gcc
423ae2b273f4: Pull complete 
de83a2304fa1: Pull complete 
f9a83bce3af0: Pull complete 
b6b53be908de: Pull complete 
9cbc4fd0e0b5: Pull complete 
a44d4501cb15: Pull complete 
f7b0500940ec: Pull complete 
3b83d0de910e: Pull complete 
Digest: sha256:0a5cf4a7122848d8e1b677695f0ae16891d76a018b82cb855cc6983e3feb7d07
Status: Downloaded newer image for stronglytyped/arm-none-eabi-gcc:latest
docker.io/stronglytyped/arm-none-eabi-gcc:latest
raph@raph-VirtualBox:~/iut/week_1$ docker images
REPOSITORY                        TAG                    IMAGE ID       CREATED         SIZE
stronglytyped/arm-none-eabi-gcc   latest                 9562bea71b90   23 months ago   848MB
```

The same person as the first time would probably say something again here, likely:

-"Wow, 848MB just to cross-compile something? Isn't it overkill?"

Maybe. But I did the hardest part, which was to look at a progress bar, and [memory is cheap](https://www.google.com/search?q=1TB+ssd&client=ubuntu&hs=f3b&channel=fs&biw=1244&bih=1168&tbm=shop&ei=6yDzYdjbHs2elwSmwJnoBQ&ved=0ahUKEwiY_cXIgdP1AhVNz4UKHSZgBl0Q4dUDCAo&uact=5&oq=1TB+ssd&gs_lcp=Cgtwcm9kdWN0cy1jYxADMgQIABBDMgUIABCABDIFCAAQgAQyBQgAEIAEMgQIABAYMgQIABAYMgQIABAYMgQIABAYMgQIABAYMgQIABAYOgoIABCxAxCDARBDOgsIABCABBCxAxCDAUoECEEYAVDXBli_I2DRI2gBcAB4AIAB5xaIAYYZkgEFNC45LTGYAQCgAQHAAQE&sclient=products-cc).

```
raph@raph-VirtualBox:~/iut/week_1$ docker run -it --mount src="$(pwd)",dst="/work/shared",type=bind stronglytyped/arm-none-eabi-gcc /bin/bash -c "arm-none-eabi-gcc --specs=rdimon.specs -lgcc -lc -lm -lrdimon  shared/main.c -o shared/hello_arm"
```

And the arm binary is here!

```
raph@raph-VirtualBox:~/iut/week_1$ file hello_arm
hello_arm: ELF 32-bit LSB executable, ARM, EABI5 version 1 (SYSV), statically linked, not stripped
raph@raph-VirtualBox:~/iut/week_1$ ./hello_arm
bash: ./hello_arm: cannot execute binary file: Exec format error
```

Now here, even fixing the apt sources may not have been enough. This docker image contains arm gcc 9.x. Most of the time, a given Linux distribution apt only support 1-2 major versions of GNU compilers. If yours is not in the list, you have to manually install the package and dependencies from the provider.

Here is the good thing about docker: I could use this image on several different machines, and never care about the apt sources or version of the distribution I am using!

### The Python problem

Now, let's imagine we need to test a python script with 2 versions, 3.8.6 and 2.7.8.

How to do this using docker? The python script **test_me.py** is provided.

**Note**: you can look at the solution below, but work it on your own first at lest for a few minutes.


```
raph@raph-VirtualBox:~/iut/week_1$ docker run -it --mount src="$(pwd)",dst="/tmp",type=bind python:2.7.8 /bin/bash -c "python /tmp/test_me.py"
('Python version', '2.7.8 (default, Nov 26 2014, 22:28:51) \n[GCC 4.9.1]')
Integer division
('3 / 2 =', 1)
('3 // 2 =', 1)
('3 / 2.0 =', 1.5)
('3 // 2.0 =', 1.0)
Unicode
("type(b'byte type does not exist') = ", <type 'str'>)****
```

On the host system, the same script would yield: 

```
raph@raph-VirtualBox:~/iut/week_1$ python3 test_me.py 
Python version 3.8.6 (default, Jan 27 2021, 15:42:20) 
[GCC 10.2.0]
Integer division
3 / 2 = 1.5
3 // 2 = 1
3 / 2.0 = 1.5
3 // 2.0 = 1.0
Unicode
type(b'byte type does not exist') =  <class 'bytes'>
```


### Automation exercice

Try to do the previous exercice for a given list of python versions of your choice, but at least 10 different ones, with 2 and 3 major.
You can use shell or python scripting.

## Author
Raphaël Bouterige (January, 2022)
