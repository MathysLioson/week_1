import sys
print('Python version', sys.version)

print('Integer division')

print('3 / 2 =', 3 / 2)
print('3 // 2 =', 3 // 2)
print('3 / 2.0 =', 3 / 2.0)
print('3 // 2.0 =', 3 // 2.0)

print('Unicode')
print("type(b'byte type does not exist') = ", type(b'byte type does not exist'))

